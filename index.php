<?php
	$bot = new BoardBot($config_file);
	$bot->loadBoard();
?>

<!DOCTYPE html>
<html>
	<head>
		<title><?= APP_NAME ?></title>
		<meta charset="UTF-8">
		<?= ViewHelpers::stylesheetIncludeTag("bootstrap-material-design.min.css","bootstrap.min.css","application.css"); ?>
		<!-- <meta http-equiv="refresh" content="120" > -->
		<style>
			@import url("<?= ASSETS_FOLDER ?>/fonts/OpenSans-ExtraBold.ttf");
		</style>
	</head>
	<body>
		<div class="row">
      <div class="container">
				<div class="jumbotron">
					<div class="message-container">
						<div class="row">
							<div class="col-md-7 cardapio">
								<h2>Cardápio</h2>
								<p>Espetinho com Refrigerante <span class="label label-primary">R$ 8,00</span></p>
								<p>Panqueca com Refrigerante <span class="label label-primary">R$ 7,00</span></p>
								<p>Duas panquecas com Refrigerante <span class="label label-primary">R$ 12,00</span></p>
								<p>Panqueca de Prestígio ou Frango <span class="label label-primary">R$ 4,50</span></p>
								<p>Espetinho <span class="label label-primary">R$ 6,00</span></p>
								<p>Refrigerante ou Suco <span class="label label-primary">R$ 3,50</span></p>
								<p>Água sem Gás <span class="label label-primary">R$ 2,00</span></p>
								<p>Água com Gás <span class="label label-primary">R$ 2,50</span></p>
								<p>Chocolate <span class="label label-primary">R$ 2,50</span></p>
								<p>Trident ou Halls <span class="label label-primary">R$ 1,50</span></p>
							</div>

							<div class="col-md-5">
								<h2>Pedidos prontos!</h2>
								<div class="alert alert-primary">
									<h2>Anteriores</h2>
									<h1><?= $bot->getBoard("last_1"); ?></h1>
									<h1><?= $bot->getBoard("last"); ?></h1>
								</div>
								<div class="alert alert-success">
									<h2>RETIRAR</h2>
									<h1><?= $bot->getBoard("actual"); ?></h1>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?= ViewHelpers::javascriptIncludeTag("jquery.js", "application.js", "bootstrap.js", "material.js"); ?>
	</body>
</html>
