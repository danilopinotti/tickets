<?php
	class BoardBot{
		private $configs;

		function __construct($config_file = "./.config"){
			$this->configs = new ConfigurationFile($config_file, "0");
		}

		//Verify if atual status from board is equivalent to telegram chat and load if not
		public function loadBoard(){			
			//Update with offline command
			if($this->configs->getConfiguration("last_board_off") != $this->configs->getConfiguration("last_board")){
				$this->configs->setConfiguration("last_board", $this->configs->getConfiguration("last_board_off"));
				//Offline configuration have priority.
				return;
			}
		}

		//Return the board file reference.
		public function getBoard($num){
			if($num == "actual")
				return $this->configs->getConfiguration("last_board");
			else
				return $this->configs->getConfiguration($num);
		}

		//This function return if have or not updates
		public static function haveUpdates($config_file){
			$configs = new ConfigurationFile($config_file);
			if ($configs->getConfiguration("last_board_off") != $configs->getConfiguration("last_board"))
				return "true";
			return "false";
		}
	}
?>