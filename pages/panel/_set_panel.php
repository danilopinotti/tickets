<?php
	$config = new ConfigurationFile($config_file);
	$actual = $config->getConfiguration("last_board_off");
?>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">
			Controle de ticket
		</h3>
	</div>
	<div class="panel-body">
		<p>Defina o valor a ser colocado:</p>
		
		<div class="container">
			<form action="change.php" method="POST">
				<div class="form-group">
					<input name="set-board" type="number" value="<?= $actual ?>" class="form-control" id="ticket_number">
				</div>
				<div class="form-group">
					<input type="submit" value="Salvar" class="btn btn-default">
				</div>
			</form>
		</div>
	</div>
</div>