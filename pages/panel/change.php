<?php
	$configs = new ConfigurationFile($config_file, "0");
	$last_board_off = $configs->getConfiguration("last_board_off");

	function updateLast($last, $configs){
		$configs->setConfiguration("last_1", $configs->getConfiguration("last"));
		$configs->setConfiguration("last", $last);
	}

	if(isset($_POST['change-board'])){
		if ($_POST['change-board'] == "next"){
			updateLast($last_board_off, $configs);

			$configs->setConfiguration("last_board_off", $last_board_off + 1);
			Flash::message('success','Ticket avançado para <strong>'.$configs->getConfiguration("last_board_off").'</strong>');
		}
		else if ($_POST['change-board'] == "previous" && $last_board_off > 0){
			updateLast($last_board_off, $configs);

			$configs->setConfiguration("last_board_off", $last_board_off - 1);
			Flash::message('success','Ticket regredid opara <strong>'.$configs->getConfiguration("last_board_off").'</strong>');
		}
	}
	if(isset($_POST['set-board'])){
		updateLast($last_board_off, $configs);

		$configs->setConfiguration("last_board_off", $_POST['set-board']);
		Flash::message('success','Ticket alterado para <strong>'.$configs->getConfiguration("last_board_off").'</strong>');
	}
	header("location: index.php");
?>
